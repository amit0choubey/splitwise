<?php
error_reporting(0);
class RequestValidator
{

  public function __construct()
  {
    
  }

  public static function validateFriendEmail($email,$con)
  {
    
     
    if (empty($email) || $email == '')
      $errors[] = "empty email id";
    elseif (isset($email) && !empty($email) && (is_numeric($email) && $email <= 0))
      $errors[] = "Non string email id";
	  
	  $query="SELECT COUNT(id) cnt FROM `friend` WHERE email='".$email."'";
	
	 $result = mysqli_query($con, $query);
	 
	 
	  while($row = mysqli_fetch_assoc($result)) {
		  
		  if($row['cnt'] > 0)
		  $errors[]="Email is already registered with us.";
	      
	  }
	  
    if (count($errors) > 0)
      return $errors;
    else
      return false;
  }
  
  public static function validatebill($email,$amount,$desc,$friend)
  {
    
  
    if (empty($email) || $email == '')
      $errors[] = "empty email id";
    elseif (isset($email) && !empty($email) && (is_numeric($email) && $email <= 0))
      $errors[] = "Non string email id";
	
	if (empty($amount) || $amount == '')
      $errors[] = "amount is empty";
    elseif (isset($amount) && !empty($amount) && !is_numeric($amount))
      $errors[] = "Non numeric amount";
	  
	if (empty($desc) || $desc == '')
      $errors[] = "empty desc";
    elseif (isset($desc) && !empty($desc) && (is_numeric($email) && $email <= 0))
      $errors[] = "Non string desc";
	  
	if (empty($friend) || $friend == '')
      $errors[] = "empty friend list";  
	  
	  
    if (count($errors) > 0)
      return $errors;
    else
      return false;
    
  }
  
  public static function validateCustomerId($id)
  {
    

    if (empty($id) || $id == '')
      $errors[] = "empty request id";
    elseif (isset($id) && !empty($id) && (!is_numeric($id) || $id <= 0))
      $errors[] = "Non interger custer id";
    if (count($errors) > 0)
      return $errors;
    else
      return false;
  }
  
  public static function validateDriverId($id)
  {
    

    if (empty($id) || $id == '')
      $errors[] = "empty driver id";
    elseif (isset($id) && !empty($id) && (!is_numeric($id) || $id <= 0))
      $errors[] = "Invalid driver id";
    if (count($errors) > 0)
      return $errors;
    else
      return false;
  }
  
  public static function validateDriverAssign($req_id,$driver_id)
  {
    

    if (empty($driver_id) || $driver_id == '')
      $errors[] = "empty driver id";
    elseif (isset($driver_id) && !empty($driver_id) && (!is_numeric($driver_id) || $driver_id <= 0))
      $errors[] = "Invalid driver id";
	 else  if (empty($req_id) || $req_id == '')
      $errors[] = "empty req id";
    elseif (isset($req_id) && !empty($req_id) && (!is_numeric($req_id) || $req_id <= 0))
      $errors[] = "Invalid req id";
    if (count($errors) > 0)
      return $errors;
    else
      return false;
  }
  
  

}
