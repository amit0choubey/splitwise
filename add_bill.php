<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="css/splitwise.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<form style="width:50%; margin:10px;" action="api/add_bill.php">
 <div class="container">
 <div class="form-group">
  <label for="Email">your Email ID:</label>
  <input type="text" class="form-control" name="email" required>
</div>
<div class="form-group">
  <label for="Amount">Amount</label>
  <input type="text" class="form-control" name="amount" required>
</div>
<div class="form-group">
  <label for="description">Description:</label>
  <textarea class="form-control" rows="5" id="comment" name="desc" required></textarea>
</div>
<div class="form-group">
  <label for="sel1">Select Friend:</label>
  <select class="form-control" id="sel1" multiple name="friend[]" required>
    
  </select>
</div>

<button type="submit">Add Bill</button>
</div>
</form>

<ul id="myUL_bill"> 
</ul>

</body>

<script src="js/splitwise.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
